package pendu;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * <b>ControleurFenetreAccueil est la classe repr�sentant Le contr�leur de la premiere fenetre du pendu, celle du "menu".</b>
 * <p>
 * Ce controleur est caract�ris� par les informations suivantes :
 * <ul>
 * <li>Le stage principal � envoyer aux autres contr�leurs afin de la modifier.</li>
 * <li>Le jeu, succeptible de changer si l'on modifie les param�tres du jeu.</li>
 * <li>Les options par d�faut modifiables lors d'un changement de param�tres/d'options.</li>
 * <li>Une image d�fini dans un fichier FXML que l'on modifie avec un changement de th�me du jeu.</li>
 * </ul>
 * </p>
 * 
 * @see Options
 * @see GestionJeu
 * 
 */
public class ControleurFenetreAccueil {
	
	/**
     * Le Stage de la fen�tre. Ce stage n'est pas modifiable.
     * 
     * @see ControleurFenetreAccueil#getPrimaryStage()
     * @see ControleurFenetreAccueil#setPrimaryStage(Stage)
     */
	private Stage primaryStage;
	
	/**
     * Le jeu du Pendu. Ce jeu peut �tre modifiable lors de changement de param�tres du jeu.
     * <p>
     * Pour de plus amples informations sur le jeu, regardez la
     * documentation de la classe GestionJeu.
     * </p>
     * 
     * @see GestionJeu
     * 
     * @see ControleurFenetreAccueil#getJeu()
     * @see ControleurFenetreAccueil#setJeu(GestionJeu)
     */
	private GestionJeu jeu;
	
	/**
     * Les options du Pendu. Ces options peuvent �tre modifier lors de changement de param�tres du jeu.
     * <p>
     * Pour de plus amples informations sur les options, regardez la
     * documentation de la classe Options.
     * </p>
     * 
     * @see Options
     * 
     * @see ControleurFenetreAccueil#getOptions()
     * @see ControleurFenetreAccueil#setOptions(Options)
     */
	private Options opt;
	
	/**
     * L'image de la fen�tre d'accueil du jeu du pendu. Cette image est modifiable.
     */
	@FXML private ImageView _imageView;
	
	/**
     * Constructeur ControleurFenetreAccueil.
     * <p>
     * A la construction d'un objet ControleurFenetreAccueil, on obtient un jeu et on cr�� la partie en utilisant InitialiserPartie
     * Tout en utilisant les param�tres pass�s dans opt.
     * </p>
     * 
     * @param primaryStage
     *            Le Stage � modifier pour le constructeur.
     * @param jeu
     *            Le jeu cr�� pour ce pendu.
     * @param opt
     * 			  Les options � appliquer pour ce pendu
     * 
     * @see ControleurFenetreAccueil#primaryStage
     * @see ControleurFenetreAccueil#jeu
     * @see ControleurFenetreAccueil#opt
     * @see ControleurFenetreAccueil#_imageView
     */
	public ControleurFenetreAccueil(Stage primaryStage, GestionJeu jeu, Options opt) {
		setPrimaryStage(primaryStage);
		setJeu(jeu);
		setOptions(opt);
		jeu.InitialiserPartie();
	}
	
	/**
     * Initialize ControleurFenetreAccueil.
     * <p>
     * A la construction d'un objet ControleurFenetreAccueil,
     * On met � jour l'image d'introduction du th�me sauvegard� dans opt (Theme 1 dans ce cas) dans l'imageview de page FXML.
     * </p>
     * 
	 * @throws FileNotFoundException 
     * 
     * @see ControleurFenetreAccueil#_imageView
     */
	@FXML
	private void initialize() throws FileNotFoundException {
		_imageView.setImage(new Image(new FileInputStream("@../../ressource/themes/Theme"+Integer.toString(opt.getTheme() + 1) + "/Intro.png"))) ;
	}
	
	 /**
     * Retourne le stage de ControleurFenetreAccueil.
     * 
     * @return Une instance de Stage, qui correspond au stage utilis� par la fen�tre Accueil du pendu.
     */
	public Stage getPrimaryStage() {
		return primaryStage;
	}
	
	/**
     * Met � jour le stage de la fen�tre FXML.
     * 
     * @param primaryStage
     *            Le nouveau stage donn� � ControleurFenetreAccueil.
     */
	private void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	/**
     * Retourne le jeu de ControleurFenetreAccueil.
     * 
     * @return Une instance de GestionJeu, qui correspond au jeu utilis� par la fen�tre Accueil du pendu.
     * 
     * @see GestionJeu
     */
	public GestionJeu getJeu() {
		return jeu;
	}

	/**
     * Met � jour le jeu de la fen�tre FXML.
     * 
     * @param jeu
     *            La nouvelle version de jeu donn� � ControleurFenetreAccueil.
     */
	private void setJeu(GestionJeu jeu) {
		this.jeu = jeu;
	}
	
	/**
     * Retourne les options de ControleurFenetreAccueil.
     * 
     * @return Une instance de Options, qui correspond aux options/param�tres utilis�s par la fen�tre Accueil du pendu.
     * 
     * @see Options
     */
	public Options getOptions() {
		return opt;
	}

	/**
     * Met � jour les options de la fen�tre FXML.
     * 
     * @param opt
     *            Les nouvelles options donn�s � ControleurFenetreAccueil.
     */
	public void setOptions(Options opt) {
		this.opt = opt;
	}

	/**
	  * <p>
	  * jouer est appel�e lorsque l'on clique sur le bouton jouer de la page d'accueil.
	  * Cette m�thode change Le contr�leur de la fen�tre FXML, puis cr�� une instance de la page de FenetrePendu.fxml.
	  * Ensuite, on setScene la Stage principale avec l'instance de la FenetrePendu.fxml.
	  * Cr�ation d'une fonction handle qui regarde les Event Clavier afin de sauvegarder et afficher les caract�res saisis 
	  * si ils sont justes et utilis�s une seule et unique fois 
	  * </p>
	  * 
	  * @throws IOException
	  * 
	  * @see ControleurFenetreAccueil#primaryStage
	  * @see ControleurFenetreAccueil#jeu
	  * 
	  * @see ControleurFenetrePendu#verifierLettre(char)
	  * @see ControleurFenetrePendu#desactiverLettre(char)
	  */
	@FXML
	public void jouer() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("FenetrePendu.fxml"));
		ControleurFenetrePendu cont_Pendu = new ControleurFenetrePendu(primaryStage, jeu, opt);
		loader.setController(cont_Pendu);
		GridPane root = loader.load();
		root.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
		Scene scene = new Scene(root, 600, 900);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		root.requestFocus();
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent key) {
            	if (!jeu.ToutTrouve() && !jeu.MaxErreursDepasse()) {
            		if (key.getText().length() >= 1) { // Une obligation car les touches Ctrl Shift ... font 0 de long et sont quand m�me reconnues par l'�v�nement
    	            	char reponse = Character.toUpperCase(key.getText().charAt(0));
    	            	if (Character.isLetter(reponse)) {
    	            		if (jeu.getLettresDejaDonnees().indexOf(reponse) == -1) { // La lettre n'a pas �t� donn�e
    	            			cont_Pendu.verifierLettre(reponse);
    	                		cont_Pendu.desactiverLettre(reponse);
    	            		}
    	            	}
                	}
            	}
            }
        });
	}
	
	/**
	  * <p>
	  * quitterAccueil est appel�e lorsque l'on clique sur le bouton quitter de la page d'accueil.
	  * Cette m�thode ferme toutes les fen�tres de l'application pendu
	  * </p>
	  */
	@FXML
	public void quitterAccueil() {
		Platform.exit();
	}
	
	/**
	  * <p>
	  * parametreAccueil est appel�e lorsque l'on clique sur le bouton param�tre de la page d'accueil.
	  * Cette m�thode change les param�tres du jeu, le nombre de vie, la difficult� des mots ou bien m�me le th�me du jeu.
	  * On cr�� une boite de dialogue contenant la FenetreParametre.fxml en modifiant Le contr�leurFenetreAccueil
	  * pour ControleurFenetreParametre, puis trois boutons :
	  * 	valider pour sauvergarder dans la classe Options les param�tres voulus
	  * 	defaut pour remettre les param�tres de base
	  * 	annuler pour quitter la fen�tre des param�tres sans garder les parametres modifi�s
	  * Cette boite de dialogue attend une reponse avant de se fermer (showAndWait())
	  * </p>
	  * 
	  * @throws IOException
	  * 
	  * @see ControleurFenetreAccueil#primaryStage
	  * @see ControleurFenetreAccueil#jeu
	  * @see ControleurFenetreAccueil#opt
	  * 
	  * @see ControleurFenetreParametre
	  * @see ControleurFenetreParametre#validerParametre(int)
	  * @see ControleurFenetreParametre#defaut()
	  * @see ControleurFenetreParametre#quitter()
	  */
	@FXML
	private void parametreAccueil() throws IOException {
		Dialog<Options> dialog = new Dialog<>();
		dialog.getDialogPane().setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
		 
		FXMLLoader loader = new FXMLLoader(getClass().getResource("FenetreParametre.fxml"));

		ControleurFenetreParametre cont_Settings = new ControleurFenetreParametre(jeu, opt);
		
		loader.setController(cont_Settings);
		GridPane grille = loader.load();
		
		dialog.getDialogPane().setContent(grille);   
		ButtonType buttonTypeValider = new ButtonType("Valider", ButtonData.OK_DONE);
		ButtonType buttonTypeDefaut = new ButtonType("Defaut", ButtonData.APPLY);
		ButtonType buttonTypeAnnuler = new ButtonType("Annuler", ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(buttonTypeValider, buttonTypeDefaut, buttonTypeAnnuler);
		 
		dialog.setResultConverter(new Callback<ButtonType, Options>() {
		    @Override
		    public Options call(ButtonType b) {
		 
		        if (b == buttonTypeAnnuler) {
		            return (cont_Settings.quitter());
		        }else if (b == buttonTypeDefaut){
		        	return cont_Settings.defaut();
		        }
		        
		        return cont_Settings.validerParametre(0);
		    }
		});
		Optional<Options> result = dialog.showAndWait();
		setOptions(result.get()); //modification de l'instance d'Options avec les resultats retourner dans le call
		try {
			//Mise � jour de l'image
			_imageView.setImage(new Image(new FileInputStream("@../../ressource/themes/Theme"+Integer.toString(opt.getTheme() + 1) + "/Intro.png"))) ;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
				
    }
	
	/**
	  * <p>
	  * reglesAccueil est appel�e lorsque l'on clique sur le bouton r�gle de la page d'accueil.
	  * Cette m�thode ouvre une nouvelle fen�tre afin de pouvoir lire les regles tout en jouant
	  * Le contr�leur de la fen�tre est modifi� pour ControleurFenetreRegle.
	  * </p>
	  * 
	  * @throws IOException
	  * 
	  * @see ControleurFenetreRegle
	  */
	@FXML
	private void reglesAccueil() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("FenetreRegle.fxml"));
		ControleurFenetreRegle cont_Regle = new ControleurFenetreRegle();
		loader.setController(cont_Regle);
        Stage stage = new Stage();
        GridPane root = loader.load();
        root.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        Scene scene = new Scene(root, 400, 600);
        stage.setScene(scene);
        stage.show();
    }
	
	/**
	  * <p>
	  * aProposAccueil est appel�e lorsque l'on clique sur le bouton A Propos de la page d'accueil.
	  * Cette m�thode ouvre une nouvelle fenetre afin de pouvoir lire les "A propos" tout en jouant
	  * Le contr�leur de la fen�tre est modifi� pour ControleurFenetreAPropos.
	  * </p>
	  * 
	  * @throws IOException
	  * 
	  * @see ControleurFenetreAPropos
	  */
	@FXML
	private void aProposAccueil() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("FenetreAPropos.fxml"));
		ControleurFenetreAPropos cont_APropos = new ControleurFenetreAPropos();
		loader.setController(cont_APropos);
        Stage stage = new Stage();
        GridPane root = loader.load();
        root.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        Scene scene = new Scene(root, 400, 500);
        stage.setScene(scene);
        stage.show();
    }
	
}
