package pendu;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.stage.Stage;

/**
 * <b>ControleurFenetreAPropos est la classe repr�sentant le controleur de la fenetre � propos du pendu.</b>
 */
public class ControleurFenetreAPropos {
	
	/**
     * Constructeur ControleurFenetreAPropos.
     */
	public ControleurFenetreAPropos() {}
	
	/**
     * Initialize ControleurFenetreAPropos.
     * Fait dans le fichier FXML
     */
	@FXML
	private void initialize() {}
	
	/**
	  * <p>
	  * quitterAPropos est appel�e lorsque l'on clique sur le bouton quitter de la page A Propos.
	  * Cette m�thode ferme uniquement la fenetre utilis� pour A Propos.
	  * </p>
	  * 
	  * @param event
	  * 			  action r�alis�e avec un clic sur le bouton quitter	
	  */
	@FXML
	public void quitterAPropos(ActionEvent event) {
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		window.close();
	}
	
}
