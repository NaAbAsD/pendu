package pendu;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;
import java.util.Random;
import java.util.Vector;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * <b>ControleurFenetrePendu est la classe repr�sentant le contr�leur de la fen�tre de jeu du pendu, celle du "jeu".</b>
 * <p>
 * Ce contr�leur est caract�ris� par les informations suivantes :
 * <ul>
 * <li>Le stage principale � envoyer aux autres controleurs afin de la modifier.</li>
 * <li>Le jeu, succeptible de changer si l'on modifie les param�tres du jeu.</li>
 * <li>Les options par d�faut modifiables lors d'un changement de param�tres/d'options.</li>
 * <li>Une image d�finie dans un fichier FXML que l'on modifie avec un changement de th�me du jeu.</li>
 * <li>Un tableau d�fini dans un fichier FXML, il repr�sente la premiere ligne du clavier virtuel</li>
 * <li>Un tableau d�fini dans un fichier FXML, il repr�sente la deuxieme ligne du clavier virtuel</li>
 * <li>Un tableau d�fini dans un fichier FXML, il repr�sente la troisieme ligne du clavier virtuel</li>
 * <li>Un label d�fini dans un fichier FXML, il repr�sente le mot cach� (celui � deviner)</li>
 * <li>Un label d�fini dans un fichier FXML, il repr�sente "Mot � deviner"</li>
 * <li>Un label d�fini dans un fichier FXML, il repr�sente le nombre d'erreur</li>
 * <li>Un label d�fini dans un fichier FXML, il repr�sente le message d'encouragement ou de resultat (victoire-defaite)</li>
 * </ul>
 * </p>
 * 
 * @see Options
 * @see GestionJeu
 * 
 */
public class ControleurFenetrePendu {

	/**
     * La Stage de la fen�tre. Cet stage n'est pas modifiable.
     * 
     * @see ControleurFenetrePendu#getPrimaryStage()
     * @see ControleurFenetrePendu#setPrimaryStage(Stage)
     */
	private Stage primaryStage;
	
	/**
     * Le jeu du Pendu. Ce jeu peut �tre modifiable lors de changement de param�tres du jeu.
     * <p>
     * Pour de plus amples informations sur le jeu, regardez la
     * documentation de la classe GestionJeu.
     * </p>
     * 
     * @see GestionJeu
     * 
     * @see ControleurFenetrePendu#getJeu()
     * @see ControleurFenetrePendu#setJeu(GestionJeu)
     */
	private GestionJeu jeu;
	
	/**
     * Les options du Pendu. Ces options peuvent �tre modifier lors de changement de param�tres du jeu.
     * <p>
     * Pour de plus amples informations sur les options, regardez la
     * documentation de la classe Options.
     * </p>
     * 
     * @see Options
     * 
     * @see ControleurFenetrePendu#getOptions()
     * @see ControleurFenetrePendu#setOptions(Options)
     */
	private Options opt;
	
	/**
     * La GridPane permettant d'activer/d�sactiver les boutons de la premiere ligne du clavier.
     */
	@FXML private GridPane _hautClavier;

	/**
     * La GridPane permettant d'activer/d�sactiver les boutons de la deuxieme ligne du clavier.
     */
	@FXML private GridPane _milieuClavier;

	/**
     * La GridPane permettant d'activer/d�sactiver les boutons de la troisieme ligne du clavier.
     */
	@FXML private GridPane _basClavier;

	/**
     * Le Label permettant de mettre � jour le mot cach�.
     */
	@FXML private Label _motCache;

	/**
     * Le Label permettant de mettre � jour la couleur du texte "Mot � trouver :".
     */
	@FXML private Label _motATrouver;

	/**
     * Le Label permettant de mettre � jour le nombre d'erreur et la couleur de ce texte.
     */
	@FXML private Label _nbErreur;

	/**
     * Le Label permettant de mettre � jour le message d'encouragement/de r�sultat.
     */
	@FXML private Label _resultat;

	/**
     * L'ImageView permettant de mettre � jour l'image du pendu/du theme.
     */
	@FXML private ImageView _imageView;

	/**
     * Constructeur ControleurFenetrePendu.
     * <p>
     * A la construction d'un objet ControleurFenetrePendu, on set le stage, le jeu avec ces parametres modifi�s ou non 
     * et les options en utilisant les param�tres pass�s dans opt.
     * </p>
     * 
     * @param primaryStage
     *            La Stage � modifier pour le constructeur.
     * @param jeu
     *            Le jeu cr�� pour ce pendu.
     * @param opt
     * 			  Les options � appliquer pour ce pendu
     * 
     * @see ControleurFenetrePendu#primaryStage
     * @see ControleurFenetrePendu#jeu
     * @see ControleurFenetrePendu#opt
     */
	public ControleurFenetrePendu(Stage primaryStage, GestionJeu jeu, Options opt) {
		setPrimaryStage(primaryStage);
		setJeu(jeu);
		setOptions(opt);
	}

	/**
     * Initialize ControleurFenetrePendu.
     * <p>
     * A la construction d'un objet ControleurFenetrePendu,
     * On cr�� un mot al�atoire du dictionnaire � decouvrir,
     * On change les couleurs des labels et des images en fonction du theme s�lectionn� dans les Options.
     * </p>
     * 
	 * @throws FileNotFoundException 
	 * 
	 * @see Options
     * 
     * @see ControleurFenetrePendu#jeu
     * @see ControleurFenetrePendu#opt
     * @see ControleurFenetrePendu#_resultat
     * @see ControleurFenetrePendu#_motCache
     * @see ControleurFenetrePendu#_motATrouver
     * @see ControleurFenetrePendu#_imageView
     */
	@FXML
	private void initialize() throws FileNotFoundException {
		defaultMotCache(jeu.getMotMystere());
		_resultat.setWrapText(true);
		actualiserCouleur(opt.getTheme(), _resultat);
		actualiserCouleur(opt.getTheme(), _motCache);
		actualiserCouleur(opt.getTheme(), _motATrouver);
		// Affichage de l'image 0 (initiale) du theme indiqu� dans la classe Options
		actualiserImage(0, _imageView, "@../../ressource/themes/Theme"+Integer.toString(opt.getTheme() + 1));
	}

	/**
     * Retourne le stage de ControleurFenetrePendu.
     * 
     * @return Une instance de Stage, qui correspond � le stage utilis� par la Fenetre Pendu du pendu.
     */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	/**
     * Met � jour le stage de la fen�tre FXML.
     * 
     * @param primaryStage
     *            La nouvelle stage donn� � ControleurFenetrePendu.
     */
	private void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	/**
     * Retourne le jeu de ControleurFenetrePendu.
     * 
     * @return Une instance de GestionJeu, qui correspond au jeu utilis� par la fen�tre Pendu du pendu.
     * 
     * @see GestionJeu
     */
	public GestionJeu getJeu() {
		return jeu;
	}

	/**
     * Met � jour le jeu de la fen�tre FXML.
     * 
     * @param jeu
     *            La nouvelle version de jeu donn� � ControleurFenetrePendu.
     */
	private void setJeu(GestionJeu jeu) {
		this.jeu = jeu;
	}

	/**
     * Retourne les options de ControleurFenetrePendu.
     * 
     * @return Une instance de Options, qui correspond aux options/param�tres utilis�s par la Fenetre Pendu du pendu.
     * 
     * @see Options
     */
	public Options getOptions() {
		return opt;
	}

	/**
     * Met � jour les options de la fen�tre FXML.
     * 
     * @param opt
     *            Les nouvelles options donn�s � ControleurFenetrePendu.
     */
	public void setOptions(Options opt) {
		this.opt = opt;
	}

	/**
	  * <p>
	  * rejouer est appel�e lorsque l'on clique sur le bouton rejouer de la page du pendu.
	  * On actualise l'image et les couleurs des differents label en fonction du theme des Options.
	  * On recr�� un autre mot al�atoire du dictionnaire pour le donner au jeu puis on r�initialise le nombre d'erreur et les lettres deja trouv�s. 
	  * Cette m�thode change le controleur de la fen�tre FXML, puis cr�� une nouvelle instance de la page de FenetrePendu.fxml.
	  * Ensuite, on setScene la Stage principale avec l'instance de la FenetrePendu.fxml.
	  * Cr�ation d'une fonction handle qui regarde les Event Clavier afin de sauvegarder et afficher les caract�res saisis 
	  * si ils sont justes et utilis�s une seule et unique fois 
	  * </p>
	  * 
	  * @throws IOException
	  * 
	  * @see Options
	  * @see GestionJeu
	  * 
	  * @see ControleurFenetreAccueil#primaryStage
	  * @see ControleurFenetreAccueil#jeu
      * @see ControleurFenetrePendu#opt
      * @see ControleurFenetrePendu#_resultat
      * @see ControleurFenetrePendu#_motCache
      * @see ControleurFenetrePendu#_motATrouver
      * @see ControleurFenetrePendu#_imageView
	  * 
	  * @see ControleurFenetrePendu#verifierLettre(char)
	  * @see ControleurFenetrePendu#desactiverLettre(char)
	  */
	@FXML
	public void rejouer() throws IOException {
		actualiserCouleur(opt.getTheme(), _resultat);
		actualiserCouleur(opt.getTheme(), _motCache);
		actualiserCouleur(opt.getTheme(), _motATrouver);
		_resultat.getStyleClass().add("outline");
		_motCache.getStyleClass().add("outline");
		_motATrouver.getStyleClass().add("outline");
		try {
			actualiserImage(0, _imageView, "@../../ressource/themes/Theme"+Integer.toString(opt.getTheme() + 1));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		jeu.ChoixMotMystere();
		defaultMotCache(jeu.getMotMystere());
		jeu.setLettresDejaDonnees("");
		jeu.setNbLettresTrouvees(0);
		jeu.setNbErreurs(0);
		FXMLLoader loader = new FXMLLoader(getClass().getResource("FenetrePendu.fxml"));
		ControleurFenetrePendu cont_Pendu = new ControleurFenetrePendu(primaryStage, jeu, opt);
		loader.setController(cont_Pendu);
		GridPane root = loader.load();
		root.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
		Scene scene = new Scene(root, 600, 900);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent key) {
                if (!jeu.ToutTrouve() && !jeu.MaxErreursDepasse()) {
                    if (key.getText().length() >= 1) { // Une obligation car les touches Ctrl Shift ... font 0 de long et sont quand m�me reconnues par l'�v�nement
                        char reponse = Character.toUpperCase(key.getText().charAt(0));
                        if (Character.isLetter(reponse)) {
                            if (jeu.getLettresDejaDonnees().indexOf(reponse) == -1) { // La lettre n'a pas �t� donn�e
                                cont_Pendu.verifierLettre(reponse);
                                cont_Pendu.desactiverLettre(reponse);
                            }
                        }
                    }
                }
            }
        });
	}

	/**
	  * <p>
	  * quitterPendu est appel�e lorsque l'on clique sur le bouton quitter de la page du Pendu.
	  * Cette m�thode ferme toutes les fenetres de l'application pendu si on veut quitter le jeu ou ferme seulement la boite de dialogue si on annule
	  * </p>
	  */
	@FXML
	public void quitterPendu() {
		Alert dialogA = new Alert(AlertType.WARNING);
		dialogA.getButtonTypes().setAll(ButtonType.OK, ButtonType.NO);
		dialogA.setTitle("Avertissement");
		dialogA.setHeaderText("Voulez vous vraiment quitter la partie en cours ?");
		Optional<ButtonType> reponse = dialogA.showAndWait();
		if (reponse.get() == ButtonType.OK) {
		    Platform.exit();
		} else {
			dialogA.close();
		}
	}

	/**
	  * <p>
	  * parametrePendu est appel�e lorsque l'on clique sur le bouton param�tre de la page d'accueil.
	  * Cette m�thode change les param�tres du jeu, le nombre de vie, la difficult� des mots ou bien m�me le th�me du jeu.
	  * On cr�� une boite de dialogue contenant la FenetreParametre.fxml en modifiant le ControleurFenetrePendu
	  * pour ControleurFenetreParametre, puis trois boutons :
	  * 	valider pour sauvergarder dans la classe Options les param�tres voulus
	  * 	defaut pour remettre les param�tres de bases
	  * 	annuler pour quitter la fen�tre des param�tres sans garder les parametres modifi�s
	  * Cette boite de dialogue attend une reponse avant de se fermer (showAndWait())
	  * </p>
	  * 
	  * @throws IOException
	  * 
	  * 
	  * @see ControleurFenetrePendu#primaryStage
	  * @see ControleurFenetrePendu#jeu
	  * @see ControleurFenetrePendu#opt
	  * 
	  * @see GestionJeu
	  * @see Options
	  * @see ControleurFenetreParametre
	  * @see ControleurFenetreParametre#validerParametre(int)
	  * @see ControleurFenetreParametre#defaut()
	  * @see ControleurFenetreParametre#quitter()
	  */
	@FXML
	public void parametrePendu() throws IOException {
		Dialog<Options> dialog = new Dialog<>();
		dialog.getDialogPane().setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));

		FXMLLoader loader = new FXMLLoader(getClass().getResource("FenetreParametre.fxml"));

		ControleurFenetreParametre cont_Settings = new ControleurFenetreParametre(jeu, opt);

		loader.setController(cont_Settings);
		GridPane grille = loader.load();

		dialog.getDialogPane().setContent(grille);
		ButtonType buttonTypeValider = new ButtonType("Valider", ButtonData.OK_DONE);
		ButtonType buttonTypeDefaut = new ButtonType("Defaut", ButtonData.APPLY);
		ButtonType buttonTypeAnnuler = new ButtonType("Annuler", ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(buttonTypeValider, buttonTypeDefaut, buttonTypeAnnuler);

		dialog.setResultConverter(new Callback<ButtonType, Options>() {
		    @Override
		    public Options call(ButtonType b) {
		        if (b == buttonTypeAnnuler) {
		            return (cont_Settings.quitter());
		        }else if (b == buttonTypeDefaut){
		        	return cont_Settings.defaut();
		        }
		        return cont_Settings.validerParametre(1);
		    }
		});

		Optional<Options> result = dialog.showAndWait();
		if(result.get().isOptionsModifiees()){
			//On verifie que les Options soit � modifier (si on valide) et si oui on modifie l'instance d'Options � faire de les garder et on rejoue
			setOptions(result.get());
			try {
				rejouer();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

    }

	/**
	  * <p>
	  * reglesPendu est appel�e lorsque l'on clique sur le bouton r�gle de la page du pendu.
	  * Cette m�thode ouvre une nouvelle fen�tre afin de pouvoir lire les r�gles tout en jouant
	  * Le contr�leur de la fen�tre est modifi� pour ControleurFenetreRegle.
	  * </p>
	  * 
	  * @throws IOException
	  * 
	  * @see ControleurFenetreRegle
	  */
	@FXML
	public void reglesPendu() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("FenetreRegle.fxml"));
		ControleurFenetreRegle cont_Regle = new ControleurFenetreRegle();
		loader.setController(cont_Regle);
        Stage stage = new Stage();
        GridPane root = loader.load();
        root.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        Scene scene = new Scene(root, 400, 600);
        stage.setScene(scene);
        stage.show();
    }

	/**
	  * <p>
	  * aProposPendu est appel�e lorsque l'on clique sur le bouton A Propos de la page du pendu.
	  * Cette m�thode ouvre une nouvelle fen�tre afin de pouvoir lire les "A propos" tout en jouant
	  * Le contr�leur de la fen�tre est modifi� pour ControleurFenetreAPropos.
	  * </p>
	  * 
	  * @throws IOException
	  * 
	  * @see ControleurFenetreAPropos
	  */
	@FXML
	public void aProposPendu() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("FenetreAPropos.fxml"));
		ControleurFenetreAPropos cont_APropos = new ControleurFenetreAPropos();
		loader.setController(cont_APropos);
        Stage stage = new Stage();
        GridPane root = loader.load();
        root.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        Scene scene = new Scene(root, 400, 500);
        stage.setScene(scene);
        stage.show();
    }

	/**
	  * <p>
	  * actionClavierVisuel est appel�e lorsque l'on clique sur un des boutons du clavier vituel de la page du pendu.
	  * On verifie ensuite si la lettre du bouton a d�ja �t� s�lectionn� par le clavier virtuel ou le clavier physique.
	  * </p>
	  * 
	  * @param event
	  * 		   Cet ActionEvent est l'�v�nement de clic sur le clavier virtuel
	  */
	@FXML
	private void actionClavierVisuel(ActionEvent event) {
		Button button = (Button)event.getSource();
		verifierLettre(button.getText().charAt(0));
		button.setDisable(true);
	}

	/**
	  * <p>
	  * Fonction permettant de v�rifier que la lettre est dans le mot � trouver
	  * Si la lettre n'est pas dans le mot
	  * 	La fonction verifie d'abord que le nombre d'erreur maximum ne soit pas d�pass�,
	  * 	si il est depass�, on modifie le label et l'imageview ainsi que les couleurs du nombre d'erreur. On bloque finalement le clavier
	  * 	Si il est pas d�pass�, on modifie les couleurs du label et modifie le nombre d'erreurs ainsi que l'image correspondant aux nombres d'erreurs
	  * Sinon (la lettre est dans le mot)
	  * 	On verifie si le mot a �t� trouv�. Si il a �t� trouv�, on modifie le mot cach�, on affiche le message de victoire et l'image de victoire
	  * 	Sinon on modifie le mot cach� et on affiche un message d'encouragement
	  * </p>
	  * 
	  * @param reponse
	  * 		   Le caract�re � verifier
	  * 
	  * @see ControleurFenetrePendu#jeu
	  * @see ControleurFenetrePendu#_nbErreur
	  * @see ControleurFenetrePendu#_resultat
	  * @see ControleurFenetrePendu#_imageView
	  * @see ControleurFenetrePendu#_motCache
	  */
	public void verifierLettre(char reponse) {
		Vector<Integer> pos = new Vector<Integer>();
		jeu.MemoriserLettreChoisie(reponse);
		if (jeu.ChercherLettreDansMot(reponse, pos) == 0) { // Lettre pas dans le mot
			jeu.MAJNbErreurs();
			int nombreErreur = jeu.getNbErreurs();
			int nombreMaxErreur = jeu.getNbMaxErreurs() + 1;
			int standard = normaliserNbErreurs(nombreErreur, nombreMaxErreur);
			if (jeu.MaxErreursDepasse()) { // Nombre d'erreur = nombre maximum d'erreurs
            	_resultat.setText("Vous perdez...\nIl fallait trouver :\n"+jeu.getMotMystere()+" !!!");
            	try { // Affichage de l'image de d�faite
					actualiserImage(standard, _imageView, "@../../ressource/themes/Theme"+Integer.toString(opt.getTheme() + 1));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
            	// Modification de la couleur du nombre d'erreur et modification du nombre d'erreus afin de voir que nous avons fait le max d'erreurs
            	labelHPColorModifier(standard, _nbErreur);
            	_nbErreur.setText(String.valueOf(nombreErreur) + " erreur(s)");
            	blocageClavier();
			} else {
				// Modification du nombre d'erreur et de la couleur du texte
            	labelHPColorModifier(standard, _nbErreur);
            	_nbErreur.setText(String.valueOf(nombreErreur) + " erreur(s)");
            	try { // Modification de l'image correspondant au nombre d'erreurs
            		actualiserImage(standard, _imageView, "@../../ressource/themes/Theme"+Integer.toString(opt.getTheme() + 1));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
            	// Message d'encouragement
            	_resultat.setText(information(false, reponse, jeu));
			}
		} else { // La lettre saisie est dans le mot
			if (jeu.ToutTrouve()) { // Le mot est trouv�
				String updated = new String();
				updated = _motCache.getText();
				for (int i : pos){
					updated = changeChar(updated, i + i, reponse);
				}
				_motCache.setText(updated);
				afficherVictoire();
				try { // Affichage de l'image de victoire selon le nombre d'erreur 0 ou plus
					if(jeu.getNbErreurs() == 0){
						_imageView.setImage(new Image(new FileInputStream("@../../ressource/themes/Theme"+Integer.toString(opt.getTheme() + 1) + "/victoire0.png"))) ;
					}else{
						_imageView.setImage(new Image(new FileInputStream("@../../ressource/themes/Theme"+Integer.toString(opt.getTheme() + 1) + "/victoire1.png"))) ;
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				blocageClavier();
	    	} else { // Nouvelle lettre � afficher
	     	   	String updated = new String();
	     	   	updated = _motCache.getText();
	     	   	for (int i : pos) {
	     	   		updated = changeChar(updated, i + i, reponse);
	     	   	}
	     	   	//modification du mot � trouver
	     	   	_motCache.setText(updated);
	     	   	//message d'encouragement
	     	   	_resultat.setText(information(true, reponse, jeu));
	        }
		}
	}
	
	/**
	  * <p>
	  * Fonction cr�ant le mot cach� sous sa forme inconnue (_ _ _ _)
	  * </p>
	  * 
	  * @param mot
	  * 		   Le String du mot � trouver afin de connaitre de nombre de caractere du mot
	  * 
	  * @see ControleurFenetrePendu#_motCache
	  */
	public void defaultMotCache(String mot) {
		String updated = new String();
		for (int i = 0; i < mot.length() - 1; i++) {
			updated += "_ ";
		}
		updated += "_";
		_motCache.setText(updated);
	}
	
	/**
	  * <p>
	  * Fonction changeant un caractere d'une phrase � un index donn� 
	  * On cr�� un tableau de caract�re du mot
	  * On remplace le caract�re � l'emplacement de l'index puis on retourne tableau de caract�res sous la forme d'un String
	  * </p>
	  * 
	  * @return Une instance de String ou le caract�re a �t� modifi�
	  * 
	  * @param chaine
	  * 		   Le String du mot � mettre � jour
	  * @param idx
	  * 		   L'entier permettant de modifier la bon caractere
	  * @param monCharRempl
	  * 		   Le Char � remplacer
	  */
	public String changeChar(String chaine, int idx, char monCharRempl) {
		char[] tab = chaine.toCharArray();
		tab[idx] = monCharRempl;
		return String.valueOf(tab);
	}

	/**
	  * <p>
	  * Converti le nombre d'erreur afin de lui faire respecter une norme pour mettre � jour les informations
	  * </p>
	  * 
	  * @return l'entier compris entre 0 et 10 qui permettra de choisir la couleur, le texte et l'image en fonction du ratio erreur/erreurMax
	  * 
	  * @param erreursActuelles
	  * 		   L'entier du nombre d'erreur r�alis� par le joueur au moment de l'appel de la fonction
	  * @param nbErreursMax
	  * 		   L'entier du nombre d'erreur maximum fix� dans les parametres
	  */
	public int normaliserNbErreurs(int erreursActuelles, int nbErreursMax) {
        // Standardise le r�sultat avec 10 erreurs
        if (erreursActuelles <= (10 % nbErreursMax)) {
            return (10/nbErreursMax + 1) * erreursActuelles;
        } else {
            return (10 % nbErreursMax) + (10/nbErreursMax) * erreursActuelles;
        }
    }

	/**
	  * <p>
	  * Fonction permettant de modifier les couleurs d'un label en fonction du nombre d'erreurs
	  * qui a �t� normalis� avec la fonction de normalisation
	  * </p>
	  * 
	  * @param convertedHP
	  * 		   L'entier du nombre normalis�
	  * @param vie
	  * 		   Le label � modifier (_nbErreur dans notre cas)
	  */
    public void labelHPColorModifier(int convertedHP, Label vie) {
        // Utilise la fonction de normalisation pour modifier la couleur du label vie
    	switch (convertedHP) {
	        case (1): {
	            vie.setTextFill(Color.web("00ff00"));
	            break;
	        }
	        case (2): {
	            vie.setTextFill(Color.web("ffff00"));
	            break;
	        }
	        case (3): {
	            vie.setTextFill(Color.web("60ff00"));
	            break;
	        }
	        case (4): {
	            vie.setTextFill(Color.web("d5ff00"));
	            break;
	        }
	        case (5): {
	            vie.setTextFill(Color.web("ffae00"));
	            break;
	        }
	        case (6): {
	            vie.setTextFill(Color.web("ff6f00"));
	            break;
	        }
	        case (7): {
	            vie.setTextFill(Color.web("ff3b00"));
	            break;
	        }
	        case (8): {
	            vie.setTextFill(Color.web("f50000"));
	            break;
	        }
	        case (9): {
	            vie.setTextFill(Color.web("6f0000"));
	            break;
	        }
	        case (10): {
	            vie.setTextFill(Color.web("800000"));
	            break;
	        }
	        default: {
	        	vie.setTextFill(Color.web("000000"));
	        }
    	}
    }

    /**
	  * <p>
	  * Bloque la totalit� des boutons du clavier virtuel lorsque l'on gagne ou perd une partie de pendu
	  * </p>
	  * 
	  * @see ControleurFenetrePendu#_basClavier
	  * @see ControleurFenetrePendu#_milieuClavier
	  * @see ControleurFenetrePendu#_hautClavier
	  */
	public void blocageClavier() {
		for (int i = 0; i < _hautClavier.getChildren().size(); i++) {
			if (!_hautClavier.getChildren().get(i).isDisabled()) {
				_hautClavier.getChildren().get(i).setDisable(true);
			}
		}
		for (int i = 0; i < _milieuClavier.getChildren().size(); i++) {
			if (!_milieuClavier.getChildren().get(i).isDisabled()) {
				_milieuClavier.getChildren().get(i).setDisable(true);
			}
		}
		for (int i = 0; i < _basClavier.getChildren().size(); i++) {
			if (!_basClavier.getChildren().get(i).isDisabled()) {
				_basClavier.getChildren().get(i).setDisable(true);
			}
		}
	}

	/**
	  * <p>
	  * Fonction permettant de d�sactiver les lettres qui ont �t� utilis�es soit par le clavier virtuel soit par le clavier physique
	  * On cr�� un bouton de test que l'on initialise grace aux gridpanes des tableaux puis l'on compare le caractere 0 du bouton
	  * avec le caractere entr�e en param�tre
	  * </p>
	  * 
	  * @param reponse
	  * 		   Le caract�re � d�sactiver
	  * 
	  * @see ControleurFenetrePendu#_basClavier
	  * @see ControleurFenetrePendu#_milieuClavier
	  * @see ControleurFenetrePendu#_hautClavier
	  */
	public void desactiverLettre(char reponse) {
		for (int i = 0; i < _hautClavier.getChildren().size(); i++) {
			Button buttonTest = (Button) _hautClavier.getChildren().get(i);
			if (buttonTest.getText().charAt(0) == reponse) {
				_hautClavier.getChildren().get(i).setDisable(true);
			}
		}
		for (int i = 0; i < _milieuClavier.getChildren().size(); i++) {
			Button buttonTest = (Button) _milieuClavier.getChildren().get(i);
			if (buttonTest.getText().charAt(0) == reponse) {
				_milieuClavier.getChildren().get(i).setDisable(true);
			}
		}
		for (int i = 0; i < _basClavier.getChildren().size(); i++) {
			Button buttonTest = (Button) _basClavier.getChildren().get(i);
			if (buttonTest.getText().charAt(0) == reponse) {
				_basClavier.getChildren().get(i).setDisable(true);
			}
		}
	}
	
	/**
	  * <p>
	  * Retourne un String d'encouragement positif ou n�gatif lorsque le joueur trouve une lettre ou fait une erreur
	  * </p>
	  * 
	  * @return String al�atoire d'encouragement
	  * 
	  * @param encouragement
	  * 		   Le bool�en permettant de savoir si l'encouragement doit etre positif ou n�gatif
	  * @param lettre
	  * 		   Le label � modifier (_nbErreur dans notre cas)
	  * @param jeu
	  * 		   La GestionJeu permettant de connaitre le mot myst�re du pendu
	  * 
	  * @see GestionJeu
	  */
	public String information(boolean encouragement, char lettre, GestionJeu jeu) {
		Random r = new Random();
		System.out.println(jeu.getMotMystere());
		String[] positif = new String[] {String.format("Il y a bien un/des %c dans ce mot\nBien jou� !", lettre),
						String.format("Plus que %d lettres � deviner !", (jeu.getMotMystere().length() - jeu.getNbLettresTrouvees()))};
		String[] negatif = new String[] {String.format("Il n'y a pas de %c dans ce mot,\ndommage !", lettre),
						String.format("Il ne vous reste plus que %d vies", jeu.getNbMaxErreurs() - jeu.getNbErreurs() + 1)};
		if (encouragement) {
			return positif[r.nextInt(positif.length)];
		} else {
			return negatif[r.nextInt(negatif.length)];
		}
	}

	/**
	  * <p>
	  * Fonction permettant d'afficher un message personnalis� lors d'une victoire en fonction du nombre d'erreur r�alis� par le joueur
	  * </p>
	  * 
	  * @see ControleurFenetrePendu#jeu
	  * @see ControleurFenetrePendu#_resultat
	  */
	public void afficherVictoire() {
		if (jeu.getNbErreurs() == 0) {
			_resultat.setText("Vous avez d�couvert le mot :" +jeu.getMotMystere()+" !!!\nSans commettre la moindre erreur !\nBravo !");
		} else if (jeu.getNbErreurs() == jeu.getNbMaxErreurs()) {
			_resultat.setText("Ouf ! Vous avez eu de la chance \nVous avez trouv� "+jeu.getMotMystere()+ " lors de votre derni�re tentative");
		} else {
			_resultat.setText("Vous avez trouv� le mot: "+jeu.getMotMystere()+".\nAvec " + jeu.getNbErreurs() + " erreurs. \nTentez d'am�liorer votre score !");
		}
	}

	/**
	  * <p>
	  * Fonction permettant de modifier l'imageview en fonction du theme et du nombre d'erreur r�alis�
	  * </p>
	  * 
	  * @param nbErreurs
	  * 		   L'entier du nombre d'erreurs r�alis� lors de l'appel de la fonction
	  * @param imageFond
	  * 		   L'imageView � changer (dans notre cas _imageView)
	  * @param dossierTheme
	  * 		   Le String permettant l'acc�s au dossier theme du pendu
	  * 
	  * @throws FileNotFoundException
	  */
	 public void actualiserImage(int nbErreurs, ImageView imageFond, String dossierTheme) throws FileNotFoundException {
	      imageFond.setImage(new Image(new FileInputStream(dossierTheme + "/" + Integer.toString(nbErreurs) + ".png"))) ;
	 }

	 /**
	  * <p>
	  * Fonction permettant d'actualiser les couleurs d'un label en fonction du th�me choisi par le joueur.
	  * Et ajout d'une outline afin que les mots soient plus visible sur les th�mes
	  * </p>
	  * 
	  * @param theme
	  * 		   L'entier representant le num�ro du th�me
	  * @param label
	  * 		   Le label � modifier
	  * 
	  * @see ControleurFenetrePendu#_motATrouver
	  * @see ControleurFenetrePendu#_motCache
	  * @see ControleurFenetrePendu#_resultat
	  */
	 public void actualiserCouleur(int theme, Label label) {
		 if (theme == 0) {
			 label.setTextFill(Color.web("000000"));
			 _resultat.getStyleClass().add("outlineTheme1");
			 _motCache.getStyleClass().add("outlineTheme1");
			 _motATrouver.getStyleClass().add("outlineTheme1MotATrouver");
		 }else {
			 label.setTextFill(Color.web("FFFFFF"));
			 _resultat.getStyleClass().add("outlineTheme2");
			 _motCache.getStyleClass().add("outlineTheme2");
			 _motATrouver.getStyleClass().add("outlineTheme2MotATrouver");
		 }
	 }

}
