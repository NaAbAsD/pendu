package pendu;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;


/**
 * <b>ControleurFenetreParametre est la classe repr�sentant le contr�leur de la fen�tre des param�tres du pendu.</b>
 * <p>
 * Ce contr�leur est caract�ris� par les informations suivantes :
 * <ul>
 * <li>Le jeu, succeptible de changer si l'on modifie les param�tres du jeu.</li>
 * <li>Les options par d�faut modifiables lors d'un changement de param�tres/d'options.</li>
 * <li>La zone de texte d�finie dans un fichier FXML, elle permet de rentrer un pseudo pour l'utilisateur</li>
 * <li>Trois radioButtons d�finis dans un fichier FXML, ils permettent de modifier la taille du texte</li>
 * <li>Une ChoiceBox d�fini dans un ficher FXML, elle permet de modifier le th�me du jeu</li>
 * <li>Deux radioButtons d�finis dans un fichier FXML, ils permettent de d'activer/d�sactiver la musique et les effets sonores</li>
 * <li>Un Slider d�fini dans un fichier FXML, il permet de modifier le nombre d'erreur maximum</li>
 * <li>Un label d�fini dans un fichier FXML, il permet d'afficher le nombre d'erreur maximum</li>
 * <li>Trois RadioButtons d�finis dans un fichier FXML, ils permettent de modifier la difficult� des mots � trouver</li>
 * <li>Un bouton d�fini dans un fichier FXML, il permet de valider les param�tres.</li>
 * </ul>
 * </p>
 * 
 * @see Options
 * @see GestionJeu
 * 
 */
public class ControleurFenetreParametre {
	
	/**
     * Le jeu du Pendu. Ce jeu peut �tre modifiable lors de changement de param�tres du jeu.
     * <p>
     * Pour de plus amples informations sur le jeu , regardez la
     * documentation de la classe GestionJeu.
     * </p>
     * 
     * @see GestionJeu
     * 
     * @see ControleurFenetreParametre#getJeu()
     * @see ControleurFenetreParametre#setJeu(GestionJeu)
     */
	private GestionJeu jeu;
	
	/**
     * Les options du Pendu. Ces options peuvent �tre modifier lors de changement de param�tres du jeu.
     * <p>
     * Pour de plus amples informations sur les options , regardez la
     * documentation de la classe Options.
     * </p>
     * 
     * @see Options
     * 
     * @see ControleurFenetreParametre#getOptions()
     * @see ControleurFenetreParametre#setOptions(Options)
     */
	private Options opt;
	
	/**
     * Le TextField permettant de sauvergarder un pseudo dans les Options.
     */
	@FXML private TextField _nom;
	
	/**
     * Les RadioButton permettant de sauvegarder une taille de texte pr�cise dans les Options.
     */
	@FXML private RadioButton _radioTaillePetite;
	@FXML private RadioButton _radioTailleMoyenne;
	@FXML private RadioButton _radioTailleGrande;
	
	/**
     * La ChoiceBox permettant de sauvegarder le th�me voulu dans les Options.
     */
	@FXML private ChoiceBox<String> _choiceBox;
	
	/**
     * Les RadioButton permettant de sauvegarder l'activation de la musique et l'activation des effets sonores dans les Options.
     */
	@FXML private RadioButton _radioMusique;
	@FXML private RadioButton _radioSonores;
	
	/**
     * Le Slider permettant de sauvegarder le nombre d'erreurs possibles dans les Options.
     */
	@FXML private Slider _slider;
	
	/**
     * Le Label affichant le nombre d'erreur que l'on a choisi de modifier.
     */
	@FXML private Label _nbErreur;
	
	/**
     * Les RadioButton permettant de sauvegarder la difficult� des mots s�lectionn�e dans les Options.
     */
	@FXML private RadioButton _radioFacile;
	@FXML private RadioButton _radioMoyen;
	@FXML private RadioButton _radioDifficile;
	
	/**
     * Le Button permettant de sauvegarder les param�tres dans les Options.
     */
	@FXML private Button _valider;
	
	/**
     * Constructeur ControleurFenetreParametre.
     * <p>
     * A la construction d'un objet ControleurFenetreParametre, on met � jour les nouvelles Options � appliquer 
     * et les parametre modifi�s de GestionJeu
     * </p>
     * 
     * @param jeu
     *            Le jeu a modifier.
     * @param opt
     * 			  Les options � appliquer pour ce pendu.
     * 
     * @see ControleurFenetreParametre#jeu
     * @see ControleurFenetreParametre#opt
     */
	public ControleurFenetreParametre(GestionJeu jeu, Options opt) {
		setJeu(jeu);
		setOptions(opt);
	}
	
	/**
     * Initialize ControleurFenetreParametre.
     * <p>
     * A la construction d'un objet ControleurFenetreParametre,
     * On donne les parametres sauvegard�s afin de les afficher sur les diff�rents labels, 
     * choiceBox, et autres entit�s de la fen�tre de param�tres.
     * </p>
     * 
	 * @throws FileNotFoundException 
     * 
     * @see ControleurFenetreParametre#_nom
     * @see ControleurFenetreParametre#_radioTaillePetite
     * @see ControleurFenetreParametre#_radioTailleMoyenne
     * @see ControleurFenetreParametre#_radioTailleGrande
     * @see ControleurFenetreParametre#_choiceBox
     * @see ControleurFenetreParametre#_radioMusique
     * @see ControleurFenetreParametre#_radioSonores
     * @see ControleurFenetreParametre#_radioFacile
     * @see ControleurFenetreParametre#_radioMoyen
     * @see ControleurFenetreParametre#_radioDifficile
     * @see ControleurFenetreParametre#_slider
     * @see ControleurFenetreParametre#_nbErreur
     */
	public void initialize() {
		// Nom
		_nom.setText(opt.getNom());
		// Taille texte
		if (opt.getTailleTexte() == Options.T_PETITE) {
			_radioTaillePetite.setSelected(true);
			_radioTailleMoyenne.setSelected(false);
			_radioTailleGrande.setSelected(false);
		} else if (opt.getTailleTexte() == Options.T_MOYENNE) {
			_radioTaillePetite.setSelected(false);
			_radioTailleMoyenne.setSelected(true);
			_radioTailleGrande.setSelected(false);
		} else {
			_radioTaillePetite.setSelected(false);
			_radioTailleMoyenne.setSelected(false);
			_radioTailleGrande.setSelected(true);
		}
		// Th�me
		_choiceBox.getItems().addAll("Theme 1","Theme 2");
		_choiceBox.getSelectionModel().select(opt.getTheme());

		// Musique et sons
		_radioMusique.setSelected(opt.isMusiqueActivee());
		_radioSonores.setSelected(opt.isSonsActives());
		// Difficult�
		if (opt.getDifficulte() == Options.D_FACILE) {
			_radioFacile.setSelected(true);
			_radioMoyen.setSelected(false);
			_radioDifficile.setSelected(false);
		} else if (opt.getDifficulte() == Options.D_MOYEN) {
			_radioFacile.setSelected(false);
			_radioMoyen.setSelected(true);
			_radioDifficile.setSelected(false);
		} else {
			_radioFacile.setSelected(false);
			_radioMoyen.setSelected(false);
			_radioDifficile.setSelected(true);
		}
		// Nombre d'erreurs
		_slider.setValue(opt.getNbMaxErreurs());
		_slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            _nbErreur.setText("Nombres d'erreurs maximum : " + Integer.toString(newValue.intValue()));
        });
	}

	/**
     * Retourne le jeu de ControleurFenetreParametre.
     * 
     * @return Une instance de GestionJeu, qui correspond au jeu utilis� par le pendu actuel.
     * 
     * @see GestionJeu
     */
	public GestionJeu getJeu() {
		return jeu;
	}
	
	/**
     * Met � jour le jeu de la fenetre FXML.
     * 
     * @param jeu
     *            La nouvelle version de jeu donn� � ControleurFenetreParametre.
     */
	private void setJeu(GestionJeu jeu) {
		this.jeu = jeu;
	}

	/**
     * Retourne les options de ControleurFenetreParametre.
     * 
     * @return Une instance de Options, qui correspond aux options/param�tres utilis�es par le pendu.
     * 
     * @see Options
     */
	public Options getOptions() {
		return opt;
	}
	
	/**
     * Met � jour les options de la fen�tre FXML.
     * 
     * @param opt
     *            Les nouvelles options donn�s � ControleurFenetreParametre.
     */
	public void setOptions(Options opt) {
		this.opt = opt;
	}
	
	/**
     * Retourne la validit� du bouton valider de ControleurFenetreParametre.
     * 
     * @return un bool�en, qui correspond � la validit� du bouton valider.
     */
	public Button getValider() {
		return _valider;
	}


	/**
     * Met � jour l'�tat des boutons afin que _radioTaillePetite soit s�lectionn�.
     * 
     * @see ControleurFenetreParametre#_radioTaillePetite
     * @see ControleurFenetreParametre#_radioTailleMoyenne
     * @see ControleurFenetreParametre#_radioTailleGrande
     */
	@FXML
	public void radioButtonTaillePetite() {
		_radioTaillePetite.setSelected(true);
		_radioTailleMoyenne.setSelected(false);
		_radioTailleGrande.setSelected(false);
	}

	/**
     * Met � jour l'�tat des boutons afin que _radioTailleMoyenne soit s�lectionn�.
     * 
     * @see ControleurFenetreParametre#_radioTaillePetite
     * @see ControleurFenetreParametre#_radioTailleMoyenne
     * @see ControleurFenetreParametre#_radioTailleGrande
     */
	@FXML
	public void radioButtonTailleMoyenne() {
		_radioTaillePetite.setSelected(false);
		_radioTailleMoyenne.setSelected(true);
		_radioTailleGrande.setSelected(false);
	}

	/**
     * Met � jour l'�tat des boutons afin que _radioTailleGrande soit s�lectionn�.
     * 
     * @see ControleurFenetreParametre#_radioTaillePetite
     * @see ControleurFenetreParametre#_radioTailleMoyenne
     * @see ControleurFenetreParametre#_radioTailleGrande
     */
	@FXML
	public void radioButtonTailleGrande() {
		_radioTaillePetite.setSelected(false);
		_radioTailleMoyenne.setSelected(false);
		_radioTailleGrande.setSelected(true);
	}

	/**
     * Met � jour l'�tat des boutons afin que _radioFacile soit s�lectionn�.
     * 
     * @see ControleurFenetreParametre#_radioFacile
     * @see ControleurFenetreParametre#_radioMoyen
     * @see ControleurFenetreParametre#_radioDifficile
     */
	@FXML
	public void radioButtonFacile() {
		_radioFacile.setSelected(true);
		_radioMoyen.setSelected(false);
		_radioDifficile.setSelected(false);
	}

	/**
     * Met � jour l'�tat des boutons afin que _radioMoyen soit s�lectionn�.
     * 
     * @see ControleurFenetreParametre#_radioFacile
     * @see ControleurFenetreParametre#_radioMoyen
     * @see ControleurFenetreParametre#_radioDifficile
     */
	@FXML
	public void radioButtonMoyen() {
		_radioFacile.setSelected(false);
		_radioMoyen.setSelected(true);
		_radioDifficile.setSelected(false);
	}

	/**
     * Met � jour l'�tat des boutons afin que _radioDifficile soit s�lectionn�.
     * 
     * @see ControleurFenetreParametre#_radioFacile
     * @see ControleurFenetreParametre#_radioMoyen
     * @see ControleurFenetreParametre#_radioDifficile
     */
	@FXML
	public void radioButtonDifficile() {
		_radioFacile.setSelected(false);
		_radioMoyen.setSelected(false);
		_radioDifficile.setSelected(true);
	}

	/**
	  * <p>
	  * defaut est activ� lorsque l'on clique sur le bouton defaut de la boite de dialogue Parametre.
	  * Cette m�thode r�-initialise les parametres et le jeu en cours puis ferme la fenetre utilis� pour Parametre.
	  * Retourne les options du ControleurFenetre qui a fait appel au ControleurFenetreParametre.
	  * </p>
      * 
      * @return Une instance de Options, qui correspond aux options/param�tres par d�faut.
      * 
      * @see Options
      * 
      * @see ControleurFenetreParametre#opt
      * @see ControleurFenetreParametre#_nom
      * @see ControleurFenetreParametre#_radioTaillePetite
      * @see ControleurFenetreParametre#_radioTailleMoyenne
      * @see ControleurFenetreParametre#_radioTailleGrande
      * @see ControleurFenetreParametre#_choiceBox
      * @see ControleurFenetreParametre#_radioMusique
      * @see ControleurFenetreParametre#_radioSonores
      * @see ControleurFenetreParametre#_radioFacile
      * @see ControleurFenetreParametre#_radioMoyen
      * @see ControleurFenetreParametre#_radioDifficile
      * @see ControleurFenetreParametre#_slider
	  */
	public Options defaut() {
		Alert dialogA = new Alert(AlertType.WARNING);
		ButtonType buttonTypeOui = new ButtonType("Oui", ButtonData.OK_DONE);
		dialogA.getButtonTypes().setAll(buttonTypeOui, ButtonType.NO);
		dialogA.setTitle("Avertissement");
		dialogA.setHeaderText("Les param�tres seront r�initialis�s !\nVoulez vous vraiment quitter la partie en cours ?");
		Optional<ButtonType> reponse = dialogA.showAndWait();
		if (reponse.get() == buttonTypeOui) {
			_nom.setText("");
			_radioTaillePetite.setSelected(false);
			_radioTailleMoyenne.setSelected(true);
			_radioTailleGrande.setSelected(false);
			_choiceBox.getSelectionModel().selectFirst();
			_radioMusique.setSelected(true);
			_radioSonores.setSelected(true);
			_slider.setValue(4);
			_radioFacile.setSelected(false);
			_radioMoyen.setSelected(true);
			_radioDifficile.setSelected(false);
			setOptions(new Options());
			opt.setOptionsModifiees(true);
			try {
				jeu.ChangerDico("dict/Normal.txt");
				opt.setDifficulte(Options.D_MOYEN);
			} catch (IOException e) { // Ce n'est pas cens� arriver si on ne touche pas aux fichiers
				e.printStackTrace();
			}
			jeu.setNbMaxErreurs(opt.getNbMaxErreurs() - 1);
		}
		else{
			opt.setOptionsModifiees(false);
		}
		return opt;
	}
	
	/**
	  * <p>
	  * validerParametre est appel�e lorsque l'on clique sur le bouton valider de la boite de dialogue Parametre.
	  * Cette m�thode sauvegarde les parametres dans une instance de Options et dans le jeu puis ferme la fenetre utilis�e pour Parametre.
	  * La partie en cours est r�initialis�e si on valide les param�tres !
	  * Retourne les options du ControleurFenetre qui a fait appel au ControleurFenetreParametre.
	  * </p>
      * 
      * @return Une instance de Options, qui correspond aux options/param�tres modifi�es par le joueur.
      * 
      * @param nbpage
      * 		  nbpage = 1 si on fait appel aux parametres avec le ControleurFenetrePendu
      * 		  nbpage = 0 si on fait appel aux parametres avec le ControleurFenetreAccueil
      * 
      * @see Options
      * 
      * @see ControleurFenetreParametre#opt
      * @see ControleurFenetreParametre#jeu
      * @see ControleurFenetreParametre#_nom
      * @see ControleurFenetreParametre#_radioTaillePetite
      * @see ControleurFenetreParametre#_radioTailleMoyenne
      * @see ControleurFenetreParametre#_radioTailleGrande
      * @see ControleurFenetreParametre#_choiceBox
      * @see ControleurFenetreParametre#_radioMusique
      * @see ControleurFenetreParametre#_radioSonores
      * @see ControleurFenetreParametre#_radioFacile
      * @see ControleurFenetreParametre#_radioMoyen
      * @see ControleurFenetreParametre#_radioDifficile
      * @see ControleurFenetreParametre#_slider
	  */
	public Options validerParametre(int nbpage) {
		 /*
		  * Si nbpage = 0, alors on valide les parametres de la page du menu
		  * Si nbpage = 1, alors on valide les parametres de la page du jeu pendu et dans ce cas on affiche une boite de dialog qui avertit la remise a z�ro du jeu
		  */
		if (nbpage == 1) {
			Alert dialogA = new Alert(AlertType.WARNING);
			ButtonType buttonTypeOui = new ButtonType("Oui", ButtonData.OK_DONE);
			dialogA.getButtonTypes().setAll(buttonTypeOui, ButtonType.NO);
			dialogA.setTitle("Avertissement");
			dialogA.setHeaderText("Voulez vous vraiment quitter la partie en cours ?");
			Optional<ButtonType> reponse = dialogA.showAndWait();
			if (reponse.get() == buttonTypeOui) {
				//On appelle la fonction afin de sauvegarder les parametres dans la classe Options
				validerParametre(0);
			}else{
				opt.setOptionsModifiees(false);
			}
		} else {
			// Nom
			/*
			 * Laisser le nom affich� dans la zone de texte -> donc faire attention � l'initialisation etc
			 */
			_nom.setText(_nom.getText());
			System.out.println(_nom.getText());
			opt.setNom(_nom.getText());
			// Musique
			if (_radioMusique.isSelected()) {
				System.out.println("Musique activ�e");
				opt.setMusiqueActivee(true);
			} else {
				System.out.println("Musique d�sactiv�e");
				opt.setMusiqueActivee(false);
			}
			if (_radioSonores.isSelected()) {
				System.out.println("Sons activ�s");
				opt.setSonsActives(true);
			} else {
				System.out.println("Sons d�sactiv�s");
				opt.setSonsActives(false);
			}
			// Taille du texte
			if (_radioTaillePetite.isSelected()) {
				System.out.println("Texte de petite taille s�lectionn�");
				opt.setTailleTexte(Options.T_PETITE);
			} else if (_radioMoyen.isSelected()) {
				System.out.println("Texte de taille moyenne s�lectionn�");
				opt.setTailleTexte(Options.T_MOYENNE);
			} else {
				System.out.println("Texte de grande taille s�lectionn�");
				opt.setTailleTexte(Options.T_GRANDE);
			}
			// Th�me
			String theme = _choiceBox.getValue();
			opt.setTheme(Character.getNumericValue(theme.charAt(theme.length() - 1)) - 1);
			// Nombre erreurs max
			jeu.setNbMaxErreurs((int)_slider.getValue() - 1);
			opt.setNbMaxErreurs((int)_slider.getValue());
			// Difficult� des mots
			try {
				if (_radioFacile.isSelected()) {
					jeu.ChangerDico("dict/Facile.txt");
					opt.setDifficulte(Options.D_FACILE);
				} else if (_radioMoyen.isSelected()) {
					jeu.ChangerDico("dict/Normal.txt");
					opt.setDifficulte(Options.D_MOYEN);
				} else {
					jeu.ChangerDico("dict/Difficile.txt");
					opt.setDifficulte(Options.D_DIFFICILE);
				}
			} catch (IOException e) { // Ce n'est pas cens� arriver si on ne touche pas aux fichiers
				e.printStackTrace();
			}
			opt.setOptionsModifiees(true);
		}
		return opt;
	}

	/**
	  * <p>
	  * quitter est appel�e lorsque l'on clique sur le bouton quitter de la boite de dialogue Parametre.
	  * Cette m�thode ferme uniquement la fenetre utilis� pour Parametre.
	  * </p>
	  * Retourne les options du ControleurFenetre qui a fait appel au ControleurFenetreParametre.
      * 
      * @return Une instance de Options, qui correspond aux options/param�tres non modifi�s.
      * 
      * @see ControleurFenetreParametre#opt
	  */
	public Options quitter() {
		opt.setOptionsModifiees(false);
		return opt;
	}
}
