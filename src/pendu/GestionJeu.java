package pendu;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

public class GestionJeu {
	
	//mot � deviner
	private String motMystere;
    
    //ensemble des mots � deviner
	private  ArrayList<String> dico;

 	//ensemble des lettres d�j� donn�es par le joueur
	private String lettresDejaDonnees;

    //nombre maximum d'erreurs autoris�es
	private int  nbMaxErreurs;

    //nombre de lettres d�j� trouv�es par le joueur
	private int nbLettresTrouvees;

    //nombre d'erreurs d�j� commises par le joueur
	private int nbErreurs;

	private Random alea; //g�n�rateur de nombre al�atoire
    
	///////////////////////////////////////////////////////////////////////////////////////////////
    //Constructeur
	///////////////////////////////////////////////////////////////////////////////////////////////
    GestionJeu(String nomDico) throws IOException{
    	this.nbMaxErreurs = 4;
    	this.nbErreurs = 0;
    	this.nbLettresTrouvees = 0;
    	lettresDejaDonnees = new String();
    	this.alea = new Random();
    	this.motMystere = new String();
    	this.dico = new ArrayList<String>();
    	ConstruireDico(nomDico);
    }
    
    
	///////////////////////////////////////////////////////////////////////////////////////////////
    //Les accesseurs/modifieurs de donn�es membres
	///////////////////////////////////////////////////////////////////////////////////////////////

    /**
	 * @return the motMystere
	 */
	public String getMotMystere() {
		return motMystere;
	}
	/**
	 * @param motMystere the motMystere to set
	 */
	public void setMotMystere(String motMystere) {
		this.motMystere = motMystere;
	}
	/**
	 * @return the lettresDejaDonnees
	 */
	public String getLettresDejaDonnees() {
		return lettresDejaDonnees;
	}
	/**
	 * @param lettresDejaDonnees the lettresDejaDonnees to set
	 */
	public void setLettresDejaDonnees(String lettresDejaDonnees) {
		this.lettresDejaDonnees = lettresDejaDonnees;
	}
	/**
	 * @return the nbMaxErreurs
	 */
	public int getNbMaxErreurs() {
		return nbMaxErreurs;
	}
	/**
	 * @param nbMaxErreurs the nbMaxErreurs to set
	 */
	public void setNbMaxErreurs(int nbMaxErreurs) {
		this.nbMaxErreurs = nbMaxErreurs;
	}
	
	/**
	 * @return the nbLettresTrouvees
	 */
	public int getNbLettresTrouvees() {
		return nbLettresTrouvees;
	}
	/**
	 * @param nbLettresTrouvees the nbLettresTrouvees to set
	 */
	public void setNbLettresTrouvees(int nbLettresTrouvees) {
		this.nbLettresTrouvees = nbLettresTrouvees;
	}
	/**
	 * @return the nbErreurs
	 */
	public int getNbErreurs() {
		return nbErreurs;
	}
	/**
	 * @param nbErreurs the nbErreurs to set
	 */
	public void setNbErreurs(int nbErreurs) {
		this.nbErreurs = nbErreurs;
	}
	
	 /**
	  * @return the dico
	  */
	public ArrayList<String> getDico() {
		return dico;
	}


	/**
	 * @param dico the dico to set
	 */
	public void setDico(ArrayList<String> dico) {
		this.dico = dico;
	}    

	///////////////////////////////////////////////////////////////////////////////////////////////
	//modification de donn�es du type gestion
	///////////////////////////////////////////////////////////////////////////////////////////////

	public void MemoriserLettreChoisie(char c){
		if (this.lettresDejaDonnees.indexOf(c)==-1)  
			this.lettresDejaDonnees += c;
	}

	public void MAJNbErreurs() {
		this.nbErreurs++;
	}

	void  ConstruireDico(String nomFichierDico) throws IOException
	{//cr�e l'ensemble des mots � deviner en consultant un fichier contenant une liste de mots
		BufferedReader lecteurAvecBuffer = null;
	    String ligne;

	    try
	      {
	    	lecteurAvecBuffer = new BufferedReader(new FileReader(nomFichierDico));
	      }
	    catch(FileNotFoundException exc)
	      {
	    	System.out.println("Erreur d'ouverture");
	      }
	    while ((ligne = lecteurAvecBuffer.readLine()) != null)
	      this.dico.add(ligne);
	    lecteurAvecBuffer.close();
	}
	
	void ChangerDico(String nomFichierDico) throws IOException
	{//remplace le dictionnaire actuel avec les mots qui se trouvent dans le fichier donn�
		this.dico.clear();
		this.ConstruireDico(nomFichierDico);
	}

	void ChoixMotMystere()
	{//choisit le mot � deviner dans l'ensemble des mot � deviner
		this.motMystere = this.dico.get(alea.nextInt(this.dico.size()));
	}


	void InitialiserPartie()
	{//initialise les donn�es pour une partie du jeu
		ChoixMotMystere();
		this.nbLettresTrouvees = 0;
		this.nbErreurs = 0;
		this.lettresDejaDonnees = "";
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////
	//tests sur des donn�es du type gestion
	///////////////////////////////////////////////////////////////////////////////////////////////
	
	boolean ToutTrouve() {
	//indique si le joueur a devin� toutes les lettres du mot � deviner
		return (this.nbLettresTrouvees == this.motMystere.length());
	}

	boolean MaxErreursDepasse() {
	//indique si le joueur a commis une erreur de trop
		return (this.nbErreurs > this.nbMaxErreurs);
	}

	boolean CaractereAutorise(String c)
	//indique si c est bien une lettre de l'alphabet
	{
		return ((c.length()==1) && Character.isLetter(c.charAt(0)));
	}
	
	boolean EstMotMystere(String mot)
	//indique si mot est le mot � deviner
	{
		return (mot.compareToIgnoreCase(this.motMystere)==0);
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	//Cherche toutes les positions o� se trouve une lettre dans le mot � deviner
	///////////////////////////////////////////////////////////////////////////////////////////////

	int ChercherLettreDansMot(char car, Vector<Integer> pos)
	{

		int index = this.motMystere.indexOf(car) ;  
		int nbpos=0;

		while (index >=  0) {  
			nbpos++;
			this.nbLettresTrouvees++;
			pos.add(index);
			index = this.motMystere.indexOf(car, index +  1) ;  
		}
		return nbpos;
	}

}

   
    
