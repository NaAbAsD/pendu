package pendu;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;


public class Main extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		try {
			GestionJeu jeu = new GestionJeu("dict/Normal.txt");
			jeu.setNbMaxErreurs(jeu.getNbMaxErreurs() - 1);
			Options opt = new Options();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("FenetreAccueil.fxml"));
			ControleurFenetreAccueil cont_Boite = new ControleurFenetreAccueil(primaryStage, jeu, opt);
			loader.setController(cont_Boite);
			GridPane root = loader.load();
			Scene scene = new Scene(root, 590, 890);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Jeu du Pendu");
			primaryStage.setResizable(false);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}