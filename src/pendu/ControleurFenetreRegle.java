package pendu;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * <b>ControleurFenetreRegle est la classe repr�sentant le controleur de la fenetre des r�gles du pendu.</b>
 * <p>
 * Ce controleur est caract�ris� par les informations suivantes :
 * <ul>
 * <li>Un label qui permet d'afficher les r�gles du pendu</li>
 * </ul>
 * </p>
 */
public class ControleurFenetreRegle {
	
	/**
     * Le label de la fenetre des regles du jeu du pendu.
     */
	@FXML private Label _label;
	
	/**
     * Constructeur ControleurFenetreRegle.
     */
	public ControleurFenetreRegle() {}
	
	/**
     * Initialize ControleurFenetreRegle.
     * Ajout du texte dans le Label afin d'optenir un meilleur rendu.
     * 
     * @see ControleurFenetreRegle#_label
     */
	@FXML
	private void initialize() {
		_label.setText("Vous devez proposer l'une des 26 lettres de l'alphabet:\n"
				+ "en cliquant sur le clavier physique de votre ordinateur\n"
				+ "ou sur le clavier virtuel directement avec la souris.\n\n"
				+ "Si la lettre propos�e fait partie une ou plusieurs fois\n"
				+ "du mot � deviner, alors vous verrez apparaitre celle ci\n"
				+ "dans le compartiment \"mot � deviner\".\n"
				+ "La lettre ne pourra plus �tre rejou�e et ne sera pas\n"
				+ "disponible au clic.\n\n"
				+ "Si la lettre propos�e ne fait pas partie du mot � deviner,\n"
				+ "vous perdez l'une de vos chances de trouvez le mot\n"
				+ "myst�re.\n"
				+ "Dans la partie \"nombre d'erreurs\" vous verrez\n"
				+ "appara�tre une partie de la potence du futur pendu !\n\n"
				+ "Vous pouvez donc gagner le jeu si vous trouvez le mot\n"
				+ "myst�re en moins de 2 � 10 erreurs et vous echappez\n"
				+ "alors � la potence !\n"
				+ "Sinon vous pouvez retenter votre chance\n"
				+ "en cliquant sur le bouton avec le logo rejouer.");
	}
	
	/**
	  * <p>
	  * quitterRegle est appel�e lorsque l'on clique sur le bouton quitter de la page R�gle.
	  * Cette m�thode ferme uniquement la fen�tre utilis�e pour R�gle.
	  * </p>
	  * 
	  * @param event
	  * 			  action r�alis�e avec un clic sur le bouton quitter	
	  */
	@FXML
	public void quitterRegle(ActionEvent event) {
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		window.close();
	}
	
}
