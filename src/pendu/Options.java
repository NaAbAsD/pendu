package pendu;

/**
 * <b>Options est la classe repr�sentant les options applicables � diff�rents contr�leurs du pendu.</b>
 * <p>
 * Cette classe est caract�ris� par les informations suivantes :
 * <ul>
 * <li>Un String qui repr�sente le nom enregistr� dans les param�tres</li>
 * <li>Un entier qui repr�sente la taille du texte enregistr� dans les param�tres</li>
 * <li>Un bool�en repr�sentant si oui ou non la musique est activ�e</li>
 * <li>Un bool�en repr�sentant si oui ou non les effets sonores sont activ�s</li>
 * <li>Un entier qui repr�sente le nombre d'erreur maximum enregistr� dans les param�tres</li>
 * <li>Un entier qui repr�sente la difficult� des mots enregistr�s dans les param�tres</li>
 * <li>Un entier qui repr�sente le th�me s�l�ctionn� dans les param�tres</li>
 * <li>Un bool�en repr�sentant si oui ou non on modifie les options</li>
 * </ul>
 * </p>
 */
public class Options {

	/**
	 * Le String permettant de garder le nom du joueur
	 */
	private String nom;
	
	/**
	 * L'entier permettant de garder la taille du texte s�l�ctionn� 
	 */
	private int tailleTexte;
	
	/**
	 * Le bool�en permettant de garder la musique activer/d�sactiver
	 */
	private boolean musiqueActivee;
	
	/**
	 * Le bool�en permettant de garder les effets sonores activer/d�sactiver
	 */
	private boolean sonsActives;
	
	/**
	 * L'entier permettant de garder le nombre maximum d'erreur
	 */
	private int nbMaxErreurs;
	
	/**
	 * L'entier permettant de garder la difficult� des mots s�l�ctionn�
	 */
	private int difficulte;
	
	/**
	 * L'entier permettant de garder le th�me s�l�ctionn�
	 */
	private int theme;
	
	/**
	 * Le bool�en permettant modifier ou non les options
	 */
	private boolean optionsModifiees = true;

	/**
	 * Les 3 constantes suivantes repr�sentent la taille du texte
	 */
	public static final int T_PETITE = 1;
	public static final int T_MOYENNE = 2;
	public static final int T_GRANDE = 3;
	
	/**
	 * Les 3 constantes suivantes repr�sentent la difficult� des mots choisie (donc un dictionnaire diff�rent)
	 */
	public static final int D_FACILE = 1;
	public static final int D_MOYEN = 2;
	public static final int D_DIFFICILE = 3;

	/**
     * Constructeur Options.
     * <p>
     * A la construction d'un objet Options, on set les diff�rents �l�ments d'Options 
     * avec des donn�es que l'on qualifiera de par d�faut 
     * </p>
     * 
     * 
     * @see Options#nom
     * @see Options#theme
     * @see Options#tailleTexte
     * @see Options#difficulte
     * @see Options#nbMaxErreurs
     * @see Options#musiqueActivee
     * @see Options#sonsActives
     */
	public Options() {
		setNom("");
		setTheme(0);
		setTailleTexte(T_MOYENNE);
		setDifficulte(D_MOYEN);
		setNbMaxErreurs(4);
		setMusiqueActivee(true);
		setSonsActives(true);
	}

	/**
     * Constructeur Options.
     * <p>
     * A la construction d'un objet Options, on set les diff�rents �l�ments d'Options 
     * avec des donn�s envoy�es par ControleurFenetreParametre et que l'on souhaite sauvegarder.
     * </p>
     * 
     * @param nom
     *			  Le String du nom de l'utilisateur
     * @param theme
     * 			  L'entier du th�me que l'utilisateur utilise
     * @param tailleTexte
     * 			  L'entier de la taille du texte que l'utilisateur utilise
     * @param difficult�
     * 			  L'entier de la difficult� des mots que l'utilisateur utilise
     * @param nbMaxErreurs
     * 			  L'entier du nombre maximum d'erreurs que l'utilisateur utilise
     * @param musiqueActivee
     * 			  Le bool�en qui dit si la musique est activ� ou d�sactiv�
     * @param sonsActives
     * 			  Le bool�en qui dit si le son est activ� ou d�sactiv�
     * 
     * @see Options#nom
     * @see Options#theme
     * @see Options#tailleTexte
     * @see Options#difficulte
     * @see Options#nbMaxErreurs
     * @see Options#musiqueActivee
     * @see Options#sonsActives
     */
	public Options(String nom, int theme, int tailleTexte, int difficult�, int nbMaxErreurs, boolean musiqueActivee, boolean sonsActives) {
		setNom(nom);
		setTheme(theme);
		setTailleTexte(tailleTexte);
		setDifficulte(difficult�);
		setNbMaxErreurs(nbMaxErreurs);
		setMusiqueActivee(musiqueActivee);
		setSonsActives(sonsActives);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getTailleTexte() {
		return tailleTexte;
	}

	public void setTailleTexte(int tailleTexte) {
		this.tailleTexte = tailleTexte;
	}

	public boolean isMusiqueActivee() {
		return musiqueActivee;
	}

	public void setMusiqueActivee(boolean musiqueActivee) {
		this.musiqueActivee = musiqueActivee;
	}

	public boolean isSonsActives() {
		return sonsActives;
	}

	public void setSonsActives(boolean sonsActives) {
		this.sonsActives = sonsActives;
	}

	public int getNbMaxErreurs() {
		return nbMaxErreurs;
	}

	public void setNbMaxErreurs(int nbMaxErreurs) {
		this.nbMaxErreurs = nbMaxErreurs;
	}

	public int getDifficulte() {
		return difficulte;
	}

	public void setDifficulte(int difficulte) {
		this.difficulte = difficulte;
	}

	public int getTheme() {
		return theme;
	}

	public void setTheme(int theme) {
		this.theme = theme;
	}

	public boolean isOptionsModifiees() {
		return optionsModifiees;
	}

	public void setOptionsModifiees(boolean optionsModifiees) {
		this.optionsModifiees = optionsModifiees;
	}
	
	@Override
	public String toString() {
		return "Options [nom=" + nom + ", tailleTexte=" + tailleTexte + ", musiqueActivee=" + musiqueActivee
				+ ", sonsActives=" + sonsActives + ", nbMaxErreurs=" + nbMaxErreurs + ", difficulte=" + difficulte
				+ ", theme=" + theme + "]";
	}

}
